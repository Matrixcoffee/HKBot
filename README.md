# HK Bot
[HK Bot](https://gitlab.com/Matrixcoffee/HKBot) is an anti-abuse bot for public [Matrix](https://matrix.org) chatrooms.

## Why does it exist?
It really shouldn't, but since some people just like to make others' lives more difficult, here we are.

The purpose of this bot is twofold. It can automatically oversee rooms and respond to abuse, based on programmable pattern rules, providing a stop-gap measure in case no human moderators are nearby. It can also automate some tedious tasks via its command interface, the primary one being the complete redaction of all of an abuser's messages.

## Warning

HK Bot contains dangerous features, is unstable and unfinished, and has only undergone very limited testing. It is provided as-is, without warranty of any kind.

## Status
**Alpha**. Rough outline is there with basic functionality, but it's very unfinished.

## Recommended Installation
```
# Prerequisites
$ apt-get install git bash python3

# Main components
$ mkdir coffee_env
$ cd coffee_env
$ git clone https://gitlab.com/Matrixcoffee/HKBot.git
$ git clone https://gitlab.com/Matrixcoffee/matrix-client-core.git
$ git clone https://gitlab.com/Matrixcoffee/transformat.git
$ git clone https://github.com/matrix-org/matrix-python-sdk.git -b v0.3.2

# ONE of the following:
$ git clone https://gitlab.com/Matrixcoffee/urllib-requests-adapter.git # (recommended)
$ apt-get install python3-requests
```
If you want to run this on a [Tails](https://tails.boum.org) system, or otherwise need to connect through a SOCKS proxy, please refer to the installation instructions for [urllib-requests-adapter](https://gitlab.com/Matrixcoffee/urllib-requests-adapter).

Configuration is currently achieved by editing the source code directly. You will also need to figure out how to actually enable mass-redaction.

Running it:
```
$ /bin/sh hkbot.sh
```

HK Bot does not know how to register a [Matrix](https://matrix.org) account, so you must provide one by other means. [Riot.im](https://riot.im/app), for example, can be used to register an account, set a displayname, avatar, etc.

May this bot solve more problems for you than it causes.

## Contributing
Anyone can contribute to the project. Please send [MR](https://gitlab.com/help/user/project/merge_requests/index.md)s, or open an issue on the [tracker](https://gitlab.com/Matrixcoffee/HKBot/issues). HK Bot has a Matrix room at [#hkbot:matrix.org](https://matrix.to/#/#faqbot:matrix.org).

## License
Copyright 2018 [@Coffee:matrix.org](https://matrix.to/#/@Coffee:matrix.org)

   > Licensed under the Apache License, Version 2.0 (the "License");
   > you may not use this file except in compliance with the License.

   > The full text of the License can be obtained from the file called [LICENSE](LICENSE).

   > Unless required by applicable law or agreed to in writing, software
   > distributed under the License is distributed on an "AS IS" BASIS,
   > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   > See the License for the specific language governing permissions and
   > limitations under the License.
