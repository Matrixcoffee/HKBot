# stdlib
import collections
import queue
import threading
import time


class IDProducer:
	def __init__(self):
		self.next_id = 0
		self.lock = threading.Lock()

	def get_id(self):
		""" A thread-safe source of unique IDs """

		with self.lock:
			the_id = self.next_id
			self.next_id += 1
		return the_id


class TaskQueue:
	""" A thread-safe priority queue for storing scheduled tasks

	It supports both priorities and groups.

	Tasks can be removed (cancelled) only by group. If a task needs to
	be individually cancellable, it should be in its own group. """

	def __init__(self):
		self.lock = threading.RLock()
		self.taskdata = collections.defaultdict(lambda: collections.defaultdict(collections.deque))
		self.task_available = threading.Event()

	def put(self, item, group=None, priority=1):
		with self.lock:
			self.taskdata[priority][group].append(item)
			self.task_available.set()

	def remove_group(self, group):
		with self.lock:
			for prio_v in self.taskdata.values():
				try:
					del prio_v[group]
				except KeyError:
					pass

	def get(self):
		self.task_available.wait()
		with self.lock:
			for p in sorted(self.taskdata):
				for q in self.taskdata[p].values():
					try:
						return q.popleft()
					except IndexError:
						pass

			self.task_available.clear()
		return None


class Task:
	def __init__(self, task, *args, **kwargs):
		self.task = task
		self.args = args
		self.kwargs = kwargs
		self.set_params()

	def set_params(self, group=None, priority=1):
		self.group = group
		self.priority = priority
		return self

	def run(self):
		return self.task(*self.args, **self.kwargs)


class TimedTask(Task):
	def run_at(self, scheduler, relative_time=None, absolute_time=None):
		self.scheduler = scheduler

		if relative_time is not None:
			absolute_time = time.time() + relative_time
		self.not_before = absolute_time
		self.schedule()
		return self

	def run(self):
		if time.time() >= self.not_before:
			return super().run()
		else:
			self.schedule()
			return None

	def schedule(self):
		self.scheduler.add(self)


class RepeatingTimedTask(TimedTask):
	def set_interval(self, interval=1):
		self.interval = interval
		return self

	def run(self):
		again = super().run()
		if again:
			self.run_at(self.scheduler, relative_time = self.interval)
		return again


class Scheduler:
	def __init__(self):
		self.keeprunning = False
		self._thread = None
		self._thread_running = threading.Lock()
		self.queue = TaskQueue()

	def _main_thread(self):
		print("Scheduler._main_thread running")

		try:
			while self.keeprunning:
				task = self.queue.get()
				if task is None:
					break
				else:
					task.run()
				time.sleep(1)
				print("---")

		finally:
			print("Scheduler._main_thread stopping")
			self._thread_running.release()

	def _start(self):
		self.keeprunning = True
		self._thread = threading.Thread(target=self._main_thread)
		self._thread.start()

	def _start_if_not_running(self):
		if self._thread_running.acquire(blocking=False):
			try:
				self._start()
			except:
				self._thread_running.release()
				raise

	def stop(self):
		self.keeprunning = False
		self.queue.task_available.set()

	# FIXME: fix signature
	def add(self, *args, **kwargs):
		if len(args) != 1 or not isinstance(args[0], Task):
			return self._add(*args, **kwargs)

		task = args[0]
		self.queue.put(task, task.group, task.priority)
		self._start_if_not_running()

	# FIXME: deprecated/obsolete
	def _add(self, task, args=[], kwargs={}, group=None, priority=1):
		self.add(Task(task, *args, **kwargs).set_params(group, priority))

	def cancel(self, group):
		print("Cancelling {!r}".format(group))
		self.queue.remove_group(group)


if __name__ == '__main__':
	s = Scheduler()

	class Test:
		def __init__(self):
			self.repeats = 3

		@staticmethod
		def test(label, delay):
			print("start", label)
			time.sleep(delay)
			print("stop", label)

		def test2(self, label, delay):
			print("start", label, self.repeats, "left")
			time.sleep(delay)
			print("stop", label, self.repeats, "left")
			self.repeats -= 1
			return self.repeats > 0

	t = Test()
	test = t.test
	test2 = t.test2

	s.add(test, ["test 1", 3], group="a", priority=1)
	s.add(test, ["test 2", 3], group="b", priority=1)
	s.add(test, ["test 3", 3], group="b", priority=0)

	time.sleep(20)

	s.add(Task(test, "xXxXx BAD xXxXx", 3).set_params(group="a", priority=1))
	s.add(Task(test, "test 5", 3).set_params(group="b", priority=1))
	s.add(Task(test, "test 6", 3).set_params(group="b", priority=0))
	TimedTask(test, "test 7", 3).set_params(group="c", priority=2).run_at(s, relative_time=10)
	RepeatingTimedTask(test2, "test 8", 3).set_params(group="c", priority=2).set_interval(3).run_at(s, relative_time=10)
	s.cancel("a")

	print("End program. Waiting for scheduled events to complete.")
