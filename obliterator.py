# stdlib
import time

# external deps
import matrix_client_core.notifier as notifier
from matrix_client_core.util import getchain

# in-tree deps
import taskscheduler


def trace(*args):
	notifier.notify(__file__, 'HKBot.obliterator.trace', args)


class Obliterator:
	def __init__(self, scheduler, sdkclient, roomid, user, priority=3, reason=""):
		self.scheduler = scheduler
		self.sdkclient = sdkclient
		self.roomid = roomid
		self.user = user
		self.priority = priority
		self.reason = reason

		self.chunk = None
		self.chunkiter = None
		self.sync_token = self.sdkclient.sync_token

	def start(self):
		t = taskscheduler.RepeatingTimedTask(self.run)
		t.set_params(group=self.roomid, priority=(self.priority + 1)).set_interval(5)
		t.run_at(self.scheduler, relative_time=5)

	def run(self):
		if self.chunk is None:
			self.chunk = self.sdkclient.api.get_room_messages(self.roomid, self.sync_token, 'b')
			self.chunkiter = iter(getchain(self.chunk, 'chunk', default=()))
			return True

		for event in self.chunkiter:
			typ = getchain(event, 'type')
			sender = getchain(event, 'sender')
			event_id = getchain(event, 'event_id')
			body = getchain(event, 'content', 'body')

			trace("Destroy: {!r} {!r} {!r}".format(typ, sender, body))

			if sender != self.user: continue
			if event_id is None: continue

			#t = taskscheduler.Task(self.sdkclient.api.redact_event, self.roomid, event_id, self.reason)
			#self.scheduler.add(t.set_params(group=self.roomid, priority=self.priority))
			trace("Would redact: {!r}".format(event_id))

			state_key = getchain(event, 'state_key')
			membership = getchain(event, 'content', 'membership')
			prev_membership = getchain(event, 'unsigned', 'prev_content', 'membership')

			if typ == 'm.room.member' and state_key == self.user and \
					membership == 'join' and prev_membership != 'join':
				# The user joined the room. Stop here.
				trace("Stopping at join event.")
				return False

			return True

		self.sync_token = getchain(self.chunk, 'end')
		if self.sync_token is None: return False

		self.chunkiter = None
		self.chunk = None
		return True
